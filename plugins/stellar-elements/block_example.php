<div class="stellarElement">

	<?php 
	if (!empty($custom_text)) { ?>
		<h2 class="stellarElementHeading">
			<?php echo esc_html( $custom_text );?>
		</h2>
	<?php }	?>	

	<ul>
	    <?php
	    $args = array( 'numberposts' => $post_count );
	    $recent_posts = wp_get_recent_posts( $args );
	    foreach( $recent_posts as $recent ){
	     echo '<li><a href="' . esc_url( get_permalink( $recent["ID"] ) ). '">' .   esc_html( $recent["post_title"] ).'</a> </li> ';
	    }
	    wp_reset_query();
	    ?>
    </ul>

    <?php
	//This is the query
	global $post;
	
	$cpt_slug = 'custom_post_type_slug'; //Replace custom_post_type_slug with your custom post type's slug
	$objects = get_posts(array(
		'post_type' => $cpt_slug,
		'post_status' => 'publish',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1
	));
	
	//Check if any posts were found
	if($objects): ?>
		<div class="customLoop">
			<?php
			//Loop through the posts
			foreach($objects as $post): setup_postdata($post);
			?>
				<div <?php post_class(); ?>>
					
					<?php if(has_post_thumbnail()): ?>                                          
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
					<?php endif; ?>

					<?php echo wp_get_attachment_image(get_field('image'), 'img-size'); ?>
					<!-- Must set image as "ID" in custom fields. -->

					<h3><?php the_title(); ?></h3>
					
					<?php if(get_field('fieldname')): ?>                                   
						<?php the_field('fieldname'); ?>
					<?php endif; ?>		

					<?php if(get_field('condition_name')): ?>                                   
					Email: <a href="mailto:<?php the_field('condition_name'); ?>"></a>
					<?php endif; ?>				
					
					<div class="exWrap">
						<?php the_excerpt(); ?>
					</div>

				</div><!-- /.post -->
			<?php
			endforeach; ?>
		</div>

		<?php
		//These are important so that the rest of your page will load properly after using this function
		wp_reset_postdata();
		wp_reset_query();
		?>
	<?php endif; ?>
</div>