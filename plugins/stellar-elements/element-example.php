<?php
namespace Elementor;

//**Stellar Dev: Notes on this process - https://developers.elementor.com/creating-a-new-widget/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//**Stellar Dev: Rename Element_Example to Element_Something - and copy paste to last line of this file
class Element_Example extends Widget_Base {

   public function get_id() {
      return 'element-example';
   }

   //**Stellar Dev: This is what the element will be called when searching elementor elements, the 2nd array field is what will be used as the plugin-name for technical use
   public function get_title() {
      return __( 'Stellar Element', 'stellar-element-example' );
   }

   //**Stellar Dev: Not sure where this is used
   public function get_name() {
      return 'StellarElementName';
   }

   public function get_icon() {
      //**Stellar Dev: Icon - use classes from FontAwesome and it should work fine https://fontawesome.com/icons 
      return 'fas fa-rocket';
   }   

   protected function _register_controls() {

      //Stellar Dev: If user should have elementor options or input fields - use these
      //8/10/21 - Below is generating an error - needs some revamp

      // $this->start_controls_section(
      //    'content_section',
      //    [
      //       'label' => __( 'Details', 'stellar-element-example' ),
      //    ]
      // );

      //    $this->add_control(
      //       'title',
      //       [
      //          'label' => __( 'Title', 'stellar-element-example' ),
      //          'type' => \Elementor\Controls_Manager::TEXT,
      //          'placeholder' => __( 'Enter your title', 'stellar-element-example' ),
      //       ]
      //    );

      //    //Works well on nobo to enter a taxonomy term ID
      //    $this->add_control(
      //       'parent_term',
      //       [
      //          'label' => __( 'Parent Taxonomy Term ID', 'nobo-block' ),
      //          'type' => Controls_Manager::TEXT,
      //          'default' => '',
      //          'title' => __( 'Enter the Parent Term ID', 'nobo-block' ),
      //          //'section' => 'section_meals_page',
      //       ]
      //    );

      //    // For custom taxonomy term selection for example - https://developers.elementor.com/query-control-autocomplete-refactoring/
      //    $document_types = ElementorPro\Plugin::elementor()->documents->get_document_types( [
      //          'show_in_library' => true,
      //       ] );
      //    $this->add_control(
      //       'template_id',
      //       [
      //          'label' => __( 'Choose Template', 'elementor-pro' ),
      //          'type' => ElementorPro\Modules\QueryControl\Module::QUERY_CONTROL_ID,
      //          'label_block' => true,
      //          'autocomplete' => [
      //             'object' => ElementorPro\Modules\QueryControl\Module::QUERY_OBJECT_LIBRARY_TEMPLATE,
      //             'query' => [
      //                'meta_query' => [
      //                   [
      //                      'key' => Elementor\Core\Base\Document::TYPE_META_KEY,
      //                      'value' => array_keys( $document_types ),
      //                      'compare' => 'IN',
      //                   ],
      //                ],
      //             ],
      //          ],
      //       ]
      //    );

         // $this->add_control(
         //    'posts_per_page',
         //    [
         //       'label' => __( 'Number of Posts', 'stellar-element-example' ),
         //       'type' => Controls_Manager::SELECT,
         //       'default' => 5,
         //       'section' => 'section_blog_posts',
         //       'options' => [
         //          1 => __( 'One', 'stellar-element-example' ),
         //          2 => __( 'Two', 'stellar-element-example' ),
         //          5 => __( 'Five', 'stellar-element-example' ),
         //          10 => __( 'Ten', 'stellar-element-example' ),
         //       ]
         //    ]
         // );

      //$this->end_controls_section();      

   }

   protected function render( $instance = [] ) {

      // get our input from the widget settings.

      // $custom_text = ! empty( $instance['title'] ) ? $instance['title'] : '';
      // $post_count = ! empty( $instance['posts_per_page'] ) ? (int)$instance['posts_per_page'] : 5;

      $settings = $this->get_settings();
      $custom_text = ! empty( $settings['title'] ) ? $settings['title'] : '';
      //$post_count = ! empty( $settings['posts_per_page'] ) ? (int)$settings['posts_per_page'] : 5;

      //**Stellar Dev: Clone and create basic file here
      include('block_example.php');
      
   }

   protected function content_template() {}

   public function render_plain_content( $instance = [] ) {}

}

//**Stellar Dev: Replace Element_Example to Element_Something from line 9
Plugin::instance()->widgets_manager->register_widget_type( new Element_Example() );