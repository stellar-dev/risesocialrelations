<?php

/**
 * Plugin Name: Stellar Elements
 * Description: Custom elements added to Elementor
 * Plugin URI: https://stellarwebstudios.com
 * Version: 1.0
 * Author: Stellar Web Studios, LLC
 * Author URI: https://stellarwebstudios.com
 * Text Domain: elementor-custom-element
 */

if ( ! defined( 'ABSPATH' ) ) exit;
class ElementorCustomElement {

   private static $instance = null;

   public static function get_instance() {
      if ( ! self::$instance )
         self::$instance = new self;
      return self::$instance;
   }

   public function init(){
      add_action( 'elementor/widgets/widgets_registered', array( $this, 'widgets_registered' ) );
   }

   public function widgets_registered() {

      if(defined('ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')){

         //Stellar Dev: Clone and Rename Element File:
         $widget_file = 'plugins/stellar-elements/element-example.php';         
         $template_file = locate_template($widget_file);
         if ( !$template_file || !is_readable( $template_file ) ) {

            //Stellar Dev: Repeat filename from above:
            $template_file = plugin_dir_path(__FILE__).'element-example.php';

         }
         if ( $template_file && is_readable( $template_file ) ) {
            require_once $template_file;
         }


         // $widget_file = 'plugins/stellar-elements/element-services.php';
         // $template_file = locate_template($widget_file);
         // if ( !$template_file || !is_readable( $template_file ) ) {
         //    $template_file = plugin_dir_path(__FILE__).'element-services.php';
         // }
         // if ( $template_file && is_readable( $template_file ) ) {
         //    require_once $template_file;
         // }


         // $widget_file = 'plugins/stellar-elements/element-portfolio.php';
         // $template_file = locate_template($widget_file);
         // if ( !$template_file || !is_readable( $template_file ) ) {
         //    $template_file = plugin_dir_path(__FILE__).'element-portfolio.php';
         // }
         // if ( $template_file && is_readable( $template_file ) ) {
         //    require_once $template_file;
         // }
      }
   }
}

ElementorCustomElement::get_instance()->init();