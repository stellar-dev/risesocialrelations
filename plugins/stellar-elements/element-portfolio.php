<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Widget_Slider extends Widget_Base {

   public function get_id() {
      return 'my-blog-posts';
   }

   public function get_title() {
      return __( 'Stellar Portfolio', 'stellar-portfolio' );
   }

   public function get_icon() {
      return 'fas fa-images';
   }

   public function get_name() {
      return 'Custom Name Portfolio';
   }

   protected function _register_controls() {

//       $this->add_control(
//          'section_blog_posts',
//          [
//             'label' => __( 'Blog Posts', 'elementor-custom-element' ),
//             'type' => Controls_Manager::SECTION,
//          ]
//       );
//
//       $this->add_control(
//          'some_text',
//          [
//             'label' => __( 'Text', 'elementor-custom-element' ),
//             //'type' => Controls_Manager::TEXT,
//             //'default' => '',
//             //'title' => __( 'Enter some text', 'elementor-custom-element' ),
//             'section' => 'section_blog_posts',
//          ]
//       );
       
       

//       $this->add_control(
//          'posts_per_page',
//          [
//             'label' => __( 'Number of Posts', 'elementor-custom-element' ),
//             'type' => Controls_Manager::SELECT,
//             'default' => 5,
//             'section' => 'section_blog_posts',
//             'options' => [
//                1 => __( 'One', 'elementor-custom-element' ),
//                2 => __( 'Two', 'elementor-custom-element' ),
//                5 => __( 'Five', 'elementor-custom-element' ),
//                10 => __( 'Ten', 'elementor-custom-element' ),
//             ]
//          ]
//       );

   }

   protected function render( $instance = [] ) {

      // get our input from the widget settings.

      // $custom_text = ! empty( $instance['some_text'] ) ? $instance['some_text'] : ' (no text was entered ) ';
      // $post_count = ! empty( $instance['posts_per_page'] ) ? (int)$instance['posts_per_page'] : 5;

      $settings = $this->get_settings();
      $custom_text = ! empty( $settings['some_text'] ) ? $settings['some_text'] : ' (no text was entered ) ';
      $post_count = ! empty( $settings['posts_per_page'] ) ? (int)$settings['posts_per_page'] : 5;

      //Sidebars
      include('block_portfolio.php');

   }

   protected function _content_template() {}

   public function render_plain_content( $instance = [] ) {}

}

//Plugin::instance()->widgets_manager->register_widget( 'Elementor\Widget_My_Custom_Elementor_Thing' );

Plugin::instance()->widgets_manager->register_widget_type( new Widget_Slider() );