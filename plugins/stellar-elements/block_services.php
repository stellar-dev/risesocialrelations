<?php

$cf_slug = 'featured_service';
 
$args = array(
    'post_type'      => 'page', //write slug of post type
    'posts_per_page' => -1,
    'post_parent'    => '719', //place here id of your parent page
    'meta_query' => array(
        array(
            'key' => $cf_slug,
            'value' => true
        )
    ), 
    'order'          => 'ASC',
    'orderby'        => 'menu_order'    
 );
 
 
$childrens = new WP_Query( $args );
 
if ( $childrens->have_posts() ) : ?>
 
    <?php while ( $childrens->have_posts() ) : $childrens->the_post(); ?>
        <div class="children">
            <a href="<?php the_permalink(); ?>" id="child-<?php the_ID(); ?>">

                <?php if(get_field('service_thumbnail')): ?>

                <?php echo wp_get_attachment_image(get_field('service_thumbnail'), 'index-categories', false, array('class' => 'alignleft', 'title' => get_the_title())); ?>
                <?php endif; ?>
                
                <div class="textBox">
                    <?php if(get_field('service_short_title')): ?>
                        <h3><?php the_field('service_short_title'); ?></h3>
                    <?php else: ?>
                        <h3><?php the_title(); ?></h3>
                    <?php endif; ?>

                    <?php the_excerpt(); ?>
                    
                    <div class="btnWrap grayBorder">
                        <div class="btn detailBtn">Details</div>
                    </div>
                </div>

            </a>
        </div>
 
    <?php    endwhile; 
            endif; 
        wp_reset_query(); 
    ?>