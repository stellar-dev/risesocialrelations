<?php /*
$args = array( 'numberposts' => $post_count );
$recent_posts = wp_get_recent_posts( $args );
foreach( $recent_posts as $recent ){
 echo '<li><a href="' . esc_url( get_permalink( $recent["ID"] ) ). '">' .   esc_html( $recent["post_title"] ).'</a> </li> ';
}
wp_reset_query(); */
?>

<?php
//This is the query
global $post;

$cpt_slug = 'portfolio_item'; //Replace custom_post_type_slug with your custom post type's slug
$objects = get_posts(array(
    'post_type' => $cpt_slug,
    'post_status' => 'publish',
    //'orderby' => 'menu_order',
    'order' => 'DESC',
    'posts_per_page' => 16
));

//Check if any posts were found
if($objects): ?>
    
    <div class="customLoop cf">
        <?php
        //Loop through the posts
        foreach($objects as $post): setup_postdata($post);
        ?>
            
            <div class="portWrap">
                <div <?php post_class('innerPort'); ?>>

                    <?php if(has_post_thumbnail()): ?>                                          
                        <?php the_post_thumbnail('portfolio'); ?>
                    <?php endif; ?>

                </div><!-- /.post -->
            </div>
        <?php endforeach; ?>
    </div>    

    <div class="slider-for">
        <?php
        //Loop through the posts
        foreach($objects as $post): setup_postdata($post);
        ?>
            <div clsas="middleGuy">
                <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" <?php post_class(); ?>>

                    <?php if(has_post_thumbnail()): ?>                                          
                        <?php the_post_thumbnail('portfolio'); ?>

                    <?php endif; ?>

                </a>
            </div>

        <?php endforeach; ?>
    </div>    

    <?php
    //These are important so that the rest of your page will load properly after using this function
    wp_reset_postdata();
    wp_reset_query();
    ?>
<?php endif; ?>