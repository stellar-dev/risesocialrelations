<?php 
/**
 * This is the template for the output of the events list widget. 
 * All the items are turned on and off through the widget admin.
 * There is currently no default styling, which is highly needed.
 *
 * You can customize this view by putting a replacement file of the same name (events-list-load-widget-display.php) in the events/ directory of your theme.
 *
 * @return string
 */

// Vars set:
// '$event->AllDay',
// '$event->StartDate',
// '$event->EndDate',
// '$event->ShowMapLink',
// '$event->ShowMap',
// '$event->Cost',
// '$event->Phone',

// Don't load directly
if ( !defined('ABSPATH') ) { die('-1'); }

$event = array();
$tribe_ecp = TribeEvents::instance();
reset($tribe_ecp->metaTags); // Move pointer to beginning of array.
foreach($tribe_ecp->metaTags as $tag){
	$var_name = str_replace('_Event','',$tag);
	$event[$var_name] = tribe_get_event_meta( $post->ID, $tag, true );
}

$event = (object) $event; //Easier to work with.

ob_start();
global $more;  
if ( !isset($alt_text) ) { $alt_text = ''; }
post_class($alt_text,$post->ID);
$class = ob_get_contents();
ob_end_clean();
?>
<li <?php echo $class ?>>
	<div class="shade">
        <article class="event">
       
			<?php if(has_post_thumbnail($post->ID, 'eventSidebar')):
			$image_id = get_post_thumbnail_id($post->ID);
			$image_url = wp_get_attachment_image_src($image_id, 'Article Block', true); ?>
			<a href="<?php echo get_permalink($post->ID) ?>" class="post-thumb">
				<img src="<?php echo $image_url[0]; ?>" alt="<?php echo $post->post_title ?>"
                class="featured-img" />
			</a>
			<?php endif; ?>
		
			<div class="event">
				<h2><a href="<?php echo get_permalink($post->ID) ?>"><?php echo $post->post_title ?></a></h2>
			</div><!-- /.event -->
			<div class="when">
				<?php
					
					
					$space = false;
					$output = '';
					echo tribe_get_start_date( $post->ID, true, 'D. M j, Y' );

					
					/*echo tribe_get_start_date( $post->ID ); 
		
				 if( tribe_is_multiday( $post->ID ) || !$event->AllDay ) {
					echo ' - ' . tribe_get_end_date($post->ID);
				 }
		
					if($event->AllDay) {
						echo ' <small>('.__('All Day','tribe-events-calendar').')</small>';
				 }*/
				 
			  ?> 
			</div><!-- /.when -->
            <!--<div class="summary"><?php // the_excerpt()  ?></div>-->
			<?php 
            //the_excerpt();
            print "<div class='summary'><p>";
                $more = 0;  
				ob_start();
    			the_content();
                $old_content = ob_get_clean();
                $new_content = strip_tags($old_content);
                echo $new_content;
            print "</p></div>";

            print "<a href='";
            the_permalink(); 
            print "' class='link'>";				
            print "Details / Comments &raquo;";
            print "</a>";
            ?>
            
            
        </article>
    </div><!-- .shade -->
</li>
<?php $alt_text = ( empty( $alt_text ) ) ? 'alt' : ''; ?>
