<?php 
/**
**	Template Name: Parent Listing Page
**	
**	Template Description: Finds and lists child pages of the current page after the page content is displayed.
**/

get_header(); 
global $post;
/**
*	Get Current page object
**/
$page = $wp_query->post->ID;

?>

<section id="main-content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1><?php the_title(); ?></h1>
			</header>
			
			<?php the_content(''); ?>
	
		</article>
	
		<?php 
			global $post;
			$postChildren =& get_children(array(
				'post_parent' => $post->ID,
				'post_type' => 'page',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC' 
			)); 
		?>
		
		<?php if($postChildren): ?>
			<div class="childrenLoop">
				<?php foreach($postChildren as $post): setup_postdata($post); ?>
	                <article id="child-<?php the_ID(); ?>" <?php post_class("cf"); ?>>
	                    <?php if(has_post_thumbnail()): ?>
	                        <?php the_post_thumbnail('listing-img', array('class' => 'alignleft')); ?>
	                    <?php endif; ?>
	                    
	                    <h2><?php the_title(); ?></h2>
	                    
	                    <?php the_excerpt(); ?>
	                </article><!-- /.child -->
				<?php endforeach; ?>
			</div>
			<?php wp_reset_postdata(); //Resets the $post variable ?>
			<?php wp_reset_query(); //Resets the WP_Query object ?>
		<?php endif; ?>

	<?php endwhile; endif; ?>

</section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>