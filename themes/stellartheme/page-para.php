<?php
/*
Template Name: Parafriend
*/
get_header(); 
global $post;
/**
*	Get Current page object
**/
$page = $wp_query->post->ID;

?>

<section id="main-content" class="fullWidth">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1><?php the_title(); ?></h1>
			</header>
			
			<?php the_content(''); ?>
	
		</article>
		
		<?php //comments_template(); ?>
	
	<?php endwhile; endif; ?>

</section>

<div class="parallax-container" data-parallax="scroll" data-image-src="<?php the_field('background_image'); ?>">
    <div class="container imageBacked">
        <?php if(get_field('lower_section_content')): ?>                                   
        <?php the_field('lower_section_content'); ?>
        <?php endif; ?>
    </div>
</div>

<section id="main-content" class="fullWidth">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h1><?php the_title(); ?></h1>
			</header>
			
			<?php the_content(''); ?>
	
		</article>
		
		<?php //comments_template(); ?>
	
	<?php endwhile; endif; ?>

</section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>