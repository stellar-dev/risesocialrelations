<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php bloginfo('name'); ?><?php wp_title( '|', true, 'left' ); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php if ( file_exists(TEMPLATEPATH .'/favicon.ico') ) : ?>
		<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
	<?php endif; ?>

	<?php if ( file_exists(TEMPLATEPATH .'/apple-touch-icon.png') ) : ?>
		<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon.png">
	<?php endif; ?>

    <!-- Animation On Scroll -->
    <!-- <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> -->

	<!-- PARALLAX SCROLLING -->
	<!-- <script src="<?php bloginfo('template_url'); ?>/js/parallax.min.js"></script> -->	

	<?php wp_head(); ?>

</head>

<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php esc_attr_e( $body_classes ); ?>">

<header id="header" class="cf">
	<div class="container">
    	<?php $front_pg = get_option('page_on_front'); //Grab the ID of the current page set to the front ?>
        
		<div id="logo">
			<!-- if a logo exists uncomment this: -->
			<!--<?php if(is_front_page()):?><h1><?php endif; ?> 
				<a href="<?php bloginfo('url'); ?>" class="logo"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>" /></a>
			<?php if(is_front_page()):?></h1><?php endif; ?>-->
			<a href="<?php bloginfo('url'); ?>/" class="site-title clear"><?php bloginfo('name'); ?></a>
		</div>
    
        <?php if(is_active_sidebar('contact')): ?>
        <!-- Header Contact Info -->
        <div class="contact">
            <?php dynamic_sidebar('contact'); ?>
        </div><!-- /.contact -->
        <?php endif; ?>
        
        <!-- ACF social icons -->
		<!-- <div class="socialDiv cf">
			<ul class="socialList"> -->

				<?php 	
				// fa-facebook-f, fa-instagram, fa-twitter, fa-youtube, fa-linkedin-in

				//You need to create these custom fields as a site options page - Use Advanced Custom Fields Plugin
				// $social_fields = array(
				// 	'site_options_facebook' => array(
				// 		'class' => 'fbLink',
				// 		'icon' => 'facebook-f',
				// 		'name' => 'Facebook',
				// 	),
				// 	'site_options_twitter' => array(
				// 		'class' => 'twLink',
				// 		'icon' => 'twitter',
				// 		'name' => 'Twitter',
				// 	),
				// 	// etc...
				// );
				
				// foreach( $social_fields as $field => $atts ):

				// 	$url = get_field( $field, 'options' ); 

				// 	if( empty( esc_url( $url ) ) ) {
				// 		continue; // bail if invalid url
				// 	}

					?>

						<!-- <li>
							<a class="<?php esc_attr_e( $atts['class'] ); ?>" href="<?php echo esc_url( $url ); ?>" title="Visit Us on <?php esc_attr_e( $atts['name'] ); ?>" target="_blank">
								<i class="fab fa-<?php esc_attr_e( $atts['icon'] ); ?>"></i>
							</a>
						</li> -->

				<?php //endforeach; ?>
	    
			<!-- </ul>
		</div> -->

	</div>
</header>
    
<nav id="main-nav">
    <div class="container">
		<?php wp_nav_menu(array( 
			'container_class' => 'menu-header', 
			'theme_location' => 'primary', 
			'menu_class' => 'menu cf'
		)); ?>
	</div>
</nav>
        
<div id="main" class="cf">
    <?php //if(!is_front_page()) : ?>
    <!--<div class="container">-->
        <!--<div class="gridRow">-->
    <?php //endif; ?>