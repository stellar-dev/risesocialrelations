
	
	<footer id="footer">
		<div class="container">
			
			<?php if(has_nav_menu('footer_menu')): ?>
				<?php wp_nav_menu(array('theme_location' => 'footer_menu', 'menu_class' => 'menu cf')); ?>
			<?php endif; ?>
			
			<div class="clear">
				<div class="footLeft">
					<?php if(is_active_sidebar('copyright')): ?>
					<div class="copyright"><?php dynamic_sidebar('copyright'); ?></div><!-- /.copyright -->
					<?php endif; ?>
				</div><!-- /.left -->
				
				<!-- 
				<div class="footRight siteBy">
					<a href="https://stellarwebstudios.com/clients/" target="_blank">About</a> 
					
					&nbsp;<span class="chillDivider">|</span>&nbsp;

					Site by <a href="https://stellarwebstudios.com" class="stellar" target="_blank">Stellar</a> 
                    	
                </div>
            	--><!-- /.right -->
	                
			</div><!-- /.clear -->

		</div>
	</footer>		

    <!-- Animation On Scroll Scripts, and Initializations -->
    <!-- <script src="https://unpkg.com/aos@next/distaos.js/"></script> -->
    <!-- <script>
        AOS.init();
    </script>     -->

	<?php wp_footer(); ?>
</body>
</html>